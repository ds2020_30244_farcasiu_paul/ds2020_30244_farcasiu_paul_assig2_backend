package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.entities.Medication;
import ro.tuc.ds2020.entities.Person;

import java.util.UUID;

public interface MedicationRepository extends JpaRepository<Medication, UUID> {

    @Transactional
    void deleteMedicationByName(String name);

    @Transactional
    @Modifying
    @Query("UPDATE Medication m SET m.effects = :effects WHERE m.name = :medicationName")
    void updateSideEffects(String effects,String medicationName);

}
