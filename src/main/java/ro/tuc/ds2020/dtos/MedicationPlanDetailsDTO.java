package ro.tuc.ds2020.dtos;


import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class MedicationPlanDetailsDTO {

    private UUID id;
    @NotNull
    private String medication;
    @NotNull
    private String interval;
    @NotNull
    private Date dateStart;
    @NotNull
    private Date dateEnd;
    @NotNull
    private String name;

    public MedicationPlanDetailsDTO() {
    }

    public MedicationPlanDetailsDTO(String medication, String interval, Date dateStart, Date dateEnd, String name)
    {
        this.medication = medication;
        this.interval = interval;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.name = name;
    }


    public MedicationPlanDetailsDTO(UUID id,String medication, String interval, Date dateStart, Date dateEnd, String name)
    {
        this.id = id;
        this.medication = medication;
        this.interval = interval;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.name = name;
    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getInterval() {
        return interval;
    }

    public void setInterval(String interval) {
        this.interval = interval;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationPlanDetailsDTO that = (MedicationPlanDetailsDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(medication, that.medication) &&
                Objects.equals(interval, that.interval) &&
                Objects.equals(dateStart, that.dateStart) &&
                Objects.equals(dateEnd, that.dateEnd) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, medication, interval, dateStart, dateEnd, name);
    }
}
