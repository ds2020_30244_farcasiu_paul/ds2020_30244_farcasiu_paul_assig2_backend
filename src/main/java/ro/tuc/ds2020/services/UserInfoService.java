package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.UserInfoDetailsDTO;
import ro.tuc.ds2020.dtos.UserInfoDetailsDTO;
import ro.tuc.ds2020.dtos.builders.UserInfoBuilder;
import ro.tuc.ds2020.dtos.builders.UserInfoBuilder;
import ro.tuc.ds2020.entities.UserInfo;
import ro.tuc.ds2020.entities.UserInfo;
import ro.tuc.ds2020.repositories.UserInfoRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserInfoService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserInfoService.class);
    private final UserInfoRepository userInfoRepository;

    @Autowired
    public UserInfoService(UserInfoRepository userInfoRepository) {
        this.userInfoRepository = userInfoRepository;
    }

    public UserInfoDetailsDTO findLoginInfo(String username, String password) {
        Optional<UserInfo> prosumerOptional = userInfoRepository.findByUsernameAndPassword(username,password);
        if (!prosumerOptional.isPresent()) {
            LOGGER.error("user was not there");
            throw new ResourceNotFoundException(UserInfo.class.getSimpleName());
        }
        return UserInfoBuilder.toUserInfoDetailsDTO(prosumerOptional.get());
    }

    public UUID insertUserInfo(UserInfoDetailsDTO userInfoDTO) {
        UserInfo userInfo = UserInfoBuilder.toEntity(userInfoDTO);
        userInfo = userInfoRepository.save(userInfo);
        LOGGER.debug("UserInfo with id {} was inserted in db", userInfo.getId());
        return userInfo.getId();
    }

    public String findName(String username)
    {
        String rezultat= userInfoRepository.findNameByUsername(username);
        if (rezultat == null)
        {
            rezultat="gresit";
        }
        return rezultat;
    }

}
