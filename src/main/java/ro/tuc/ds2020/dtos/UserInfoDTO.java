package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;


import java.util.Objects;
import java.util.UUID;

public class UserInfoDTO extends RepresentationModel<UserInfoDTO> {
    private UUID id;
    private String username;
    private String password;
    private String userType;
    private String name;

    public UserInfoDTO() {
    }

    public UserInfoDTO(UUID id, String username, String password, String userType, String name)
    {
        this.id = id;
        this.username = username;
        this.password = password;
        this.userType = userType;
        this.name = name;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserInfoDTO userInfoDTO = (UserInfoDTO) o;
        return Objects.equals(username, userInfoDTO.username) && Objects.equals(password, userInfoDTO.password) &&
                Objects.equals(userType, userInfoDTO.userType) && Objects.equals(name, userInfoDTO.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password, userType,name);
    }
}
