package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.UserInfoDetailsDTO;
import ro.tuc.ds2020.dtos.PersonDetailsDTO;
import ro.tuc.ds2020.dtos.UserInfoDetailsDTO;
import ro.tuc.ds2020.services.UserInfoService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@CrossOrigin
@RequestMapping(value = "/login")
public class UserInfoController {

    private final UserInfoService userInfoService;
    @Autowired
    public UserInfoController(UserInfoService userInfoService) {
        this.userInfoService = userInfoService;
    }

    @GetMapping(value = "/{username}/{password}")
    public ResponseEntity<UserInfoDetailsDTO> getPerson(@PathVariable("username") String username, @PathVariable("password") String password) {
        UserInfoDetailsDTO dto = userInfoService.findLoginInfo(username,password);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UUID> insertProsumer(@Valid @RequestBody UserInfoDetailsDTO userInfoDTO) {
        UUID userInfoID = userInfoService.insertUserInfo(userInfoDTO);
        return new ResponseEntity<>(userInfoID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/search/{username}")
    public ResponseEntity<String> getName(@PathVariable("username") String username) {
        String rezultat=userInfoService.findName(username);
        if (rezultat.equals("gresit"))
        return new ResponseEntity<>(rezultat, HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>("name:"+rezultat,HttpStatus.OK);
    }

}
