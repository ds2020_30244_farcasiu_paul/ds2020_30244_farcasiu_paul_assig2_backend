package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.CaregiverDetailsDTO;
import ro.tuc.ds2020.entities.Caregiver;

public class CaregiverBuilder {

    private CaregiverBuilder() {
    }

    public static CaregiverDTO toCaregiverDTO(Caregiver caregiver) {
        return new CaregiverDTO(caregiver.getId(), caregiver.getName(), caregiver.getBirth(), caregiver.getGender(), caregiver.getAddress(), caregiver.getPatients());
    }

    public static CaregiverDetailsDTO toCaregiverDetailsDTO(Caregiver caregiver) {
        return new CaregiverDetailsDTO(caregiver.getId(), caregiver.getName(), caregiver.getBirth(), caregiver.getGender(), caregiver.getAddress(), caregiver.getPatients());
    }

    public static Caregiver toEntity(CaregiverDetailsDTO caregiverDetailsDTO) {
        return new Caregiver(caregiverDetailsDTO.getName(),
                caregiverDetailsDTO.getBirth(),
                caregiverDetailsDTO.getGender(),
                caregiverDetailsDTO.getAddress(),
                caregiverDetailsDTO.getPatients());
    }
}
