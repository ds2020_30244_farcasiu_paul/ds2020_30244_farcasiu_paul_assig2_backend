package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.dtos.ActivitiesDetailsDTO;
import ro.tuc.ds2020.dtos.builders.ActivitiesBuilder;
import ro.tuc.ds2020.entities.Activities;
import ro.tuc.ds2020.repositories.ActivitiesRepository;

import java.util.UUID;

@Service
public class ActivitiesService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ActivitiesService.class);
    private final ActivitiesRepository activitiesRepository;

    @Autowired
    public ActivitiesService(ActivitiesRepository activitiesRepository) {
        this.activitiesRepository = activitiesRepository;
    }


    public UUID insertActivities(ActivitiesDetailsDTO activitiesDTO) {
        Activities activities = ActivitiesBuilder.toEntity(activitiesDTO);
        activities = activitiesRepository.save(activities);
        LOGGER.debug("Activities with id {} was inserted in db", activities.getId());
        return activities.getId();
    }


}
