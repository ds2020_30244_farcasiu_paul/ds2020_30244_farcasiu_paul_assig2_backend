package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;


import javax.persistence.Column;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class ActivitiesDTO extends RepresentationModel<MedicationDTO> {
    private UUID id;
    private UUID patient_id;
    private String activity;
    private LocalDateTime start_date;
    private LocalDateTime end_date;
    private String rule;

    public ActivitiesDTO() {
    }

    public ActivitiesDTO(UUID id, UUID patient_id, String activity,LocalDateTime start_date, LocalDateTime end_date, String rule)
    {
        this.id = id;
        this.patient_id=patient_id;
        this.activity=activity;
        this.start_date=start_date;
        this.end_date=end_date;
        this.rule=rule;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(UUID patient_id) {
        this.patient_id = patient_id;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public LocalDateTime getStart_date() {
        return start_date;
    }

    public void setStart_date(LocalDateTime start_date) {
        this.start_date = start_date;
    }

    public LocalDateTime getEnd_date() {
        return end_date;
    }

    public void setEnd_date(LocalDateTime end_date) {
        this.end_date = end_date;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        ActivitiesDTO that = (ActivitiesDTO) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(patient_id, that.patient_id) &&
                Objects.equals(activity, that.activity) &&
                Objects.equals(start_date, that.start_date) &&
                Objects.equals(end_date, that.end_date) &&
                Objects.equals(rule, that.rule);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), id, patient_id, activity, start_date, end_date, rule);
    }
}
