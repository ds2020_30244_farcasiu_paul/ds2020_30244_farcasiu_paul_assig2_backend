package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.tuc.ds2020.entities.Activities;

import java.util.UUID;

public interface ActivitiesRepository extends JpaRepository<Activities, UUID> {

}
