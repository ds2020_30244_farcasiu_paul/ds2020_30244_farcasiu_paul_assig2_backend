package ro.tuc.ds2020.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.tuc.ds2020.controllers.handlers.exceptions.model.ResourceNotFoundException;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.dtos.UserInfoDetailsDTO;
import ro.tuc.ds2020.dtos.builders.PatientBuilder;
import ro.tuc.ds2020.dtos.builders.UserInfoBuilder;
import ro.tuc.ds2020.entities.Patient;
import ro.tuc.ds2020.entities.UserInfo;
import ro.tuc.ds2020.repositories.PatientRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public List<PatientDTO> findPatients() {
        List<Patient> patientList = patientRepository.findAll();
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public List<PatientDTO> findPatientsWithName(String all) {

        List<Patient> patientList=new ArrayList<>();
        if (!all.contains(","))
        {   System.out.println(all);

            System.out.println(patientRepository.findPatientByName(all).toString());
            patientList.add(patientRepository.findPatientByName(all));}
        else{
            String[] tok=all.split(",");
            for (String abc: tok) {
                System.out.println(abc);
                patientRepository.findPatientByName(abc);
                patientList.add(patientRepository.findPatientByName(abc));
            }
        }
        return patientList.stream()
                .map(PatientBuilder::toPatientDTO)
                .collect(Collectors.toList());
    }

    public UUID insertPatient(PatientDetailsDTO patientDTO) {
        Patient patient = PatientBuilder.toEntity(patientDTO);
        patient = patientRepository.save(patient);
        LOGGER.debug("Patient with id {} was inserted in db", patient.getId());
        return patient.getId();
    }

    public void deleteByName(String name)
    { patientRepository.deletePatientByName(name);
       return;
    }

    public void updatePatientAddress(String address, String patientName) {
         patientRepository.updateAddress(address,patientName);
    }

}
