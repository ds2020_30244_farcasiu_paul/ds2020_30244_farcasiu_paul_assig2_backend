package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.dtos.PatientDetailsDTO;
import ro.tuc.ds2020.entities.Patient;

public class PatientBuilder {

    private PatientBuilder() {
    }

    public static PatientDTO toPatientDTO(Patient patient) {
        return new PatientDTO(patient.getId(), patient.getName(), patient.getBirth(), patient.getGender(), patient.getAddress(), patient.getMedical());
    }

    public static PatientDetailsDTO toPatientDetailsDTO(Patient patient) {
        return new PatientDetailsDTO(patient.getId(), patient.getName(), patient.getBirth(), patient.getGender(), patient.getAddress(), patient.getMedical());
    }

    public static Patient toEntity(PatientDetailsDTO patientDetailsDTO) {
        return new Patient(patientDetailsDTO.getName(),
                patientDetailsDTO.getBirth(),
                patientDetailsDTO.getGender(),
                patientDetailsDTO.getAddress(),
                patientDetailsDTO.getMedical());
    }
}
