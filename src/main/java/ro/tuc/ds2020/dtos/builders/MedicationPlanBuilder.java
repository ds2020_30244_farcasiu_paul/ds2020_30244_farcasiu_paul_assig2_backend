package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.MedicationPlanDTO;
import ro.tuc.ds2020.dtos.MedicationPlanDetailsDTO;
import ro.tuc.ds2020.entities.MedicationPlan;




public class MedicationPlanBuilder {

    private MedicationPlanBuilder() {
    }

    public static MedicationPlanDTO toMedicationPlanDTO(MedicationPlan medication) {
        return new MedicationPlanDTO(medication.getId(),medication.getMedication(), medication.getInterval(), medication.getDateStart(),medication.getDateEnd(), medication.getName());
    }

    public static MedicationPlanDetailsDTO toMedicationPlanDetailsDTO(MedicationPlan medication) {
        return new MedicationPlanDetailsDTO(medication.getId(),medication.getMedication(), medication.getInterval(), medication.getDateStart(),medication.getDateEnd(), medication.getName());
    }

    public static MedicationPlan toEntity(MedicationPlanDetailsDTO medication) {
        return new MedicationPlan( medication.getMedication(), medication.getInterval(), medication.getDateStart(),medication.getDateEnd(), medication.getName());
    }
}
