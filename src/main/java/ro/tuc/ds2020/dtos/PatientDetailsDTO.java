package ro.tuc.ds2020.dtos;


import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class PatientDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private Date birth;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    @NotNull
    private String medical;

    public PatientDetailsDTO() {
    }

    public PatientDetailsDTO(String name, Date birth, String gender, String address, String medical)
    {
        this.name = name;
        this.birth = birth;
        this.gender = gender;
        this.address = address;
        this.medical = medical;
    }

    public PatientDetailsDTO(UUID id, String name, Date birth, String gender, String address, String medical)
    {
        this.id = id;
        this.name = name;
        this.birth = birth;
        this.gender = gender;
        this.address = address;
        this.medical = medical;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedical() {
        return medical;
    }

    public void setMedical(String medical) {
        this.medical = medical;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDetailsDTO patientDetailsDTO = (PatientDetailsDTO) o;
        return Objects.equals(name, patientDetailsDTO.name) && Objects.equals(birth, patientDetailsDTO.birth) &&
                Objects.equals(gender, patientDetailsDTO.gender) && Objects.equals(address, patientDetailsDTO.address)
                && Objects.equals(medical, patientDetailsDTO.medical);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birth, gender, address, medical);
    }
}
