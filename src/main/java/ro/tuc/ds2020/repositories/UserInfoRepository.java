package ro.tuc.ds2020.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import ro.tuc.ds2020.entities.UserInfo;

import java.util.Optional;
import java.util.UUID;

public interface UserInfoRepository  extends JpaRepository<UserInfo, UUID> {

    @Transactional
    Optional<UserInfo> findByUsernameAndPassword(String username, String password);

    @Transactional
    String findNameByUsername(String username);

}
