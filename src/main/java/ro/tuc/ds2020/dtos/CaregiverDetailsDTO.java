package ro.tuc.ds2020.dtos;


import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class CaregiverDetailsDTO {

    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private Date birth;
    @NotNull
    private String gender;
    @NotNull
    private String address;
    @NotNull
    private String patients;

    public CaregiverDetailsDTO() {
    }

    public CaregiverDetailsDTO(String name, Date birth, String gender, String address, String patients)
    {
        this.name = name;
        this.birth = birth;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
    }

    public CaregiverDetailsDTO(UUID id, String name, Date birth, String gender, String address, String patients)
    {
        this.id = id;
        this.name = name;
        this.birth = birth;
        this.gender = gender;
        this.address = address;
        this.patients = patients;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPatients() {
        return patients;
    }

    public void setPatients(String patients) {
        this.patients = patients;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CaregiverDetailsDTO caregiverDetailsDTO = (CaregiverDetailsDTO) o;
        return Objects.equals(name, caregiverDetailsDTO.name) && Objects.equals(birth, caregiverDetailsDTO.birth) &&
                Objects.equals(gender, caregiverDetailsDTO.gender) && Objects.equals(address, caregiverDetailsDTO.address)
                && Objects.equals(patients, caregiverDetailsDTO.patients);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birth, gender, address, patients);
    }
}
