package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;


import java.util.Date;
import java.util.Objects;
import java.util.UUID;

public class PatientDTO extends RepresentationModel<MedicationDTO> {
    private UUID id;
    private String name;
    private Date birth;
    private String gender;
    private String address;
    private String medical;

    public PatientDTO() {
    }

    public PatientDTO(UUID id, String name, Date birth, String gender, String address, String medical)
    {
        this.id = id;
        this.name = name;
        this.birth = birth;
        this.gender = gender;
        this.address = address;
        this.medical = medical;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMedical() {
        return medical;
    }

    public void setMedical(String medical) {
        this.medical = medical;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PatientDTO patientDTO = (PatientDTO) o;
        return Objects.equals(name, patientDTO.name) && Objects.equals(birth, patientDTO.birth) &&
                Objects.equals(gender, patientDTO.gender) && Objects.equals(address, patientDTO.address)
                && Objects.equals(medical, patientDTO.medical);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, birth, gender, address, medical);
    }
}
