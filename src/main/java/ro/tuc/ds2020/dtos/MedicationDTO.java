package ro.tuc.ds2020.dtos;

import org.springframework.hateoas.RepresentationModel;


import java.util.Objects;
import java.util.UUID;

public class MedicationDTO extends RepresentationModel<MedicationDTO> {
    private UUID id;
    private String name;
    private String effects;
    private int dosage;

    public MedicationDTO() {
    }

    public MedicationDTO(UUID id, String name, String effects, int dosage)
    {
        this.id = id;
        this.name = name;
        this.effects = effects;
        this.dosage = dosage;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEffects() {
        return effects;
    }

    public void setEffects(String effects) {
        this.effects = effects;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MedicationDTO medicationDTO = (MedicationDTO) o;
        return Objects.equals(name, medicationDTO.name) && Objects.equals(effects, medicationDTO.effects) &&
                dosage == medicationDTO.dosage;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, effects, dosage);
    }
}
